/* Small command preprocessor for Supybot
 * (C)Copyright 2015 Dominik "Zatherz" Banaszak
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include "supystitution.h"

int getoctet(unsigned char x) {
	if ((x & 0x80) == 0) {
		return 1;
	} else if ((x & 0xE0) == 0xC0) {
		return 2;
	} else if ((x & 0xF0) == 0xE0) {
		return 3;
	} else if ((x & 0xF8) == 0xF0) {
		return 4;
	}
	return 0;
}

int main(int argc, char** argv) {
	int escape = 0;
	int backslashes = 0;
	int level = 0;
	int pos = 1;
	int* quotes;
	unsigned int quotes_length = 0;
	if (argc < 2) {
		fprintf(stderr, "error >> too few arguments\n");
		return 1;
	}
	if (argc > 2) {
		fprintf(stderr, "error >> too much arguments\n");
		return 1;
	}
	quotes = malloc(sizeof(int) * 1);
	quotes_length = 1;
	if (quotes == NULL) {
		fprintf(stderr, "error >> failed allocating memory for array\n");
		return 2;
	}
	char* string = argv[1];
	int oct = -1;
	printf("input : %s\n", string);
	printf("output: ");
	for (char* c = string; *c != '\0'; c++) {
		oct = getoctet((unsigned char)*c);
		if (oct) {
			pos++;
		}
		if (*c == '^' && !escape) {
			escape = 1;
			continue;
		}
		if (escape) {
			escape = 0;
			printf("%c", *c);
			continue;
		}
		if (*c == '<') {
			for (int i = 0; i < backslashes; i++) {
				printf("\\");
				fflush(stdout);
			}
			backslashes = backslashes * 2 + 1;
			level++;
			quotes_length++;
			quotes = realloc(quotes, sizeof(int) * quotes_length);
			if (quotes == NULL) {
				fprintf(stderr, "\nerror >> failed reallocating memory for array");
				exit(2);
			}
			quotes[quotes_length - 1] = pos;
			printf("\"");
			fflush(stdout);
			continue;
		} else if (*c == '>') {
			backslashes = backslashes / 2;
			for (int i = 0; i < backslashes; i++) {
				printf("\\");
			}
			level--;
			printf("\"");
			fflush(stdout);
			if (level < 0) {
				fprintf(stderr, "\nerror >> syntax error at character %d ('>'): trying to close quote without an opening one\n", pos);
				exit(3);
			}
			quotes_length--;
			quotes = realloc(quotes, sizeof(int) * quotes_length);
			if (quotes == NULL) {
				fprintf(stderr, "\nerror >> failed reallocating memory for array\n");
				exit(2);
			}
			continue;
		}
		printf("%c", *c);
		fflush(stdout);
	}
	if (level != 0) {
		fprintf(stderr, "\nerror >> syntax error at character %d ('<'): quote not closed\n", quotes[quotes_length - 1]);
		exit(3);
	} else {
		printf("\n");
	}
	return 0;
}
