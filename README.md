Supystitution
=============
Supystitution is a really small preprocessor for Supybot commands. The current version (1.1.0) is implemented in under 200 lines of code. All it does is replacing '<' and '>' characters with properly escaped (if needed) quotes. This allows for nesting quotes inside other quotes without a hassle.

For example, if you have an "rword" alias that selects a random word, then if you wanted to select a random sentence, you'd have to do this:

    rword "\"Hello world!\"" "\"Goodbye world!\""

This is because rword is an alias. When you execute the above command in supybot, the alias will pass "Hello world" and "Goodbye world" as two parameters to an actual command, instead of passing "Hello", "world!", "Goodbye" and "world!".

This gets more and more confusing when you have to place more quotes inside other quotes. Every next "level" must have additional 2x(number of backslashes of last level)+1 backslashes with a double quote at the end. Now, this isn't hard for the 0th level (abc, not quoted), 1st level ("abc", 0 backslashes), 2nd level ("\\"abc\\"", 1 backslash) and even the 3rd level ("\\"\\\\\\""abc\\\\\\"\\"", 3 backslashes) but it definitely gets much less readable and more confusing with next levels (level 4 is 7 backslashes, level 6 is 31 backslashes, level 7 is 63 backslashes!). This is where Supystitution really helps. Instead of you having to type every single backslash yourself, Supystitutition uses simple logic to generate these escaped quotes itself. Instead of typing the above "rword" command, all you have to do with Supystitution is to pass the following command to it:

    rword <<Hello world!>> <<Goodbye world!>>

Now, choose which one is more readable. Supystitution automagically finds out the current level and prints the specific escape sequence. All you have to then do is copy and paste the output. You can even integrate supystitution into your IRC client, so that you can for example call it with "/s". Keep the number of non-escaped '<' and '>' characters even and you won't have problems.

Supystitution uses '^' characters instead of backslashes for escaping to reduce collisions with commands. You rarely need to escape anything. Escaping prints the next character to the right as-is without applying any tests or modifications on it. For example, "^^" returns "^" etc.

You can use it to escape '<' and '>' characters to print them instead of substituting them with quotes.

Supystitution includes minimal error reporting, including syntax errors.

In case you want to put the escape symbol before '<' and '>' and STILL have the character substituted with escaped quotes, then as mentioned before just escape the escape symbol. For example, passing this:

    rword <<Hello ^<supybot^>!>> <<Goodbye ^<supybot^>!>>

will expand to:

    rword "\"Hello <supybot>!\"" "\"Goodbye <supybot>!\""

and:

    rword ^^<<Hello world!>> ^^<<Goodbye world!>>

will expand to:

    rword ^"\"Hello world!\"" ^"\"Goodbye world!\""

Where did the name come from?
=============================
The name is a mix of two words: "supybot" and "substitution". "Supybot" for obvious reasons, "substitution" because the program substitutes some characters with others.

Licensing
=========
Supystitution is licensed under GNU GPLv2. You're free to use it however you like, modify however you like and redistribute either free or paid. For more information please read the LICENSE file.
