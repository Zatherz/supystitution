DESTDIR ?= /
PREFIX ?= usr
BINDIR ?= bin
BIN_DEST ?= $(DESTDIR)/$(PREFIX)/$(BINDIR)
CC ?= clang

all: build

build: supystitution.c
	$(CC) -std=c11 $(CFLAGS) -o supystitution $^

clean: supystitution
	rm $^

install: supystitution
	install -Dm755 $^ $(BIN_DEST) 

uninstall: $(BIN_DEST)/supystitution
	rm $^
